# \\Developer/

## Run project:

Primarily, you have to make sure that all packages are installed.

If not, run command:
### `npm install` (from `/ui` directory, and `/api` directory.)

After that you have two ways for run the project:

### First way

1. Run server from `/api` directory, command:

```shell
npm start
```

2. Run client interface from `/api` directory, the same command:

```shell
npm start
```
3. Follow to ==> [http://localhost:3000](http://localhost:3000)

### Second way

1. Run server and also client interface from `/ui` directory, command:

```shell
npm run start-serve
```
2. Follow to ==> [http://localhost:4454](http://localhost:4454)

### Description:

I used additional library `material-ui` for Autocomplete in Search Box.
I think it helps clients to find server from list faster.

### Good luck =)