const url = 'http://localhost:4454/servers';

const requests = {
    getServers: function () {
        return fetch(url, {
                method: 'GET'
        })
            .then(response => response.json())
            .then((servers) => {
                return servers
            });
    },
    getServer: function (id) {
        return fetch(`${url}/${id}`, {
            method: 'GET'
        })
            .then(response => response.json())
            .then((server) => {
                return server
            });
    },
    turnOn: function (id) {
        return fetch(`${url}/${id}/on`, {
            method: 'PUT'
        })
            .then(response => response.json())
            .then((server) => {
                return server;
            });
    },
    turnOff: function (id) {
        return fetch(`${url}/${id}/off`, {
            method: 'PUT'
        })
            .then(response => response.json())
            .then((server) => {
                return server
            });
    },
    reboot: function (id) {
        return fetch(`${url}/${id}/reboot`, {
            method: 'PUT'
        })
            .then(response => response.json())
            .then((server) => {
                return server
            });
    }
}

export default requests;