import React from 'react';
import {render, screen} from "@testing-library/react";
import NumberOfServers from "./NumberOfServers";

const servers = [
    {
        "id": 1,
        "name": "US East (Virginia)",
        "status": "ONLINE"
    },
    {
        "id": 2,
        "name": "US East (Ohio)",
        "status": "ONLINE"
    },
    {
        "id": 3,
        "name": "US West (N. California)",
        "status": "OFFLINE"
    }
]

it('Number of Servers No Filter', () => {
    render(<NumberOfServers servers={servers} filter=""/>);
    screen.getByText('Number of elements: 3');
});

it('Number of Servers with Filter', () => {
    render(<NumberOfServers servers={servers} filter="Ohio"/>);
    screen.getByText('Number of elements: 1');
});

it('Number of Servers with lowercase Filter', () => {
    render(<NumberOfServers servers={servers} filter="us east"/>);
    screen.getByText('Number of elements: 2');
});