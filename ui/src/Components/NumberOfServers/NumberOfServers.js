import React from "react";

function NumberOfServers(props){
    const {servers, filter} = props
    const lowFilter = filter.toLowerCase();
    const filteredServers = servers.filter((server) => server.name.toLowerCase().indexOf(lowFilter) >= 0);

    return(
        <h4>Number of elements: {filteredServers.length}</h4>
    )
}

export default NumberOfServers;