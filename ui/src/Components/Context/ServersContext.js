import React, {createContext, useState} from 'react'

export const ServersContext = createContext('');

const ServersContextProvider = (props) => {
    const [servers, setServers] = useState(props.servers);

    return (
        <ServersContext.Provider value={{servers, setServers}}>
            {props.children}
        </ServersContext.Provider>
    )
}

export default ServersContextProvider;