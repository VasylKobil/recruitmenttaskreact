import React, {useContext, useEffect} from 'react';
import './App.css';
import Table from "../Table/Table";
import requests from "../../Services/api";
import {ServersContext} from "../Context/ServersContext";

function App() {
    const {servers, setServers} = useContext(ServersContext)

    useEffect(() => {
        const getData = async () => {
            const servers = await requests.getServers();
            setServers(servers);
        }
        getData();
    },[setServers])

    return (
      <div className="App">
        <header className="App-header">
            <h6>Recruitment task</h6>
        </header>
          {servers && <Table/>}
      </div>
    );
}

export default App;
