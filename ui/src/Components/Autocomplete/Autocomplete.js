import React, {useContext} from 'react';
import './Autocomplete.css';
import useAutocomplete from '@material-ui/lab/useAutocomplete';
import {ServersContext} from "../Context/ServersContext";

function Autocomplete(props) {
    const {servers} = useContext(ServersContext);
    const {onFilterChange} = props;

    const {
        getRootProps,
        getInputProps,
        getListboxProps,
        getOptionProps,
        groupedOptions
    } = useAutocomplete({
        options: servers,
        getOptionLabel: (option) => option.name,
        onInputChange: (event, value) => onFilterChange && onFilterChange(value),
        freeSolo: true,
    });

    return (
        <>
            <div{...getRootProps()}>
                <input placeholder={'Search'} {...getInputProps()} />
            </div>
            <div>
                {groupedOptions.length > 0 ? (
                    <ul{...getListboxProps()}>
                        {groupedOptions.map((option, index) => (
                            <li {...getOptionProps({ option, index })}>{option.name}</li>
                        ))}
                    </ul>
                ) : null}
            </div>
        </>
    );
}
export default Autocomplete;