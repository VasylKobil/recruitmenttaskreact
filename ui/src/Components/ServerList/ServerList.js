import React, {useContext} from "react";
import './ServerList.css';
import ServerActions from "../ServerActions/ServerActions";
import {ServersContext} from "../Context/ServersContext";
import {ServerStatus} from "../ServerStatus/ServerStatus";
import requests from "../../Services/api";

function ServerList(props){
    const {servers, setServers} = useContext(ServersContext);
    const filter = props.filter.toLowerCase();

    function onStatusChange(server){
        const oldServer = servers.find(it => it.id === server.id);
        oldServer.status = server.status
        setServers([...servers]);
        if(server.status === 'REBOOTING'){
            checkServer(server.id);
        }
    }

    function checkServer(serverId){
        function check(){
            setTimeout(async () => {
                const server = await requests.getServer(serverId);
                if(server.status !== 'REBOOTING'){
                    onStatusChange(server);
                }else{
                    check();
                }
            },1000)
        }
        check();
    }

    return(
        <div className="servers">
            <ul>
                <li>
                    <div><span>NAME</span></div>
                    <div><span>STATUS</span></div>
                </li>
                {servers && servers
                    .filter((server) => server.name.toLowerCase().indexOf(filter) >= 0)
                    .map((server) => (
                    <li key={server.id}>
                        <div><span>{server.name}</span></div>
                        <ServerStatus status={server.status}/>
                        <ServerActions
                            serverId={server.id}
                            status={server.status}
                            onStatusChange={onStatusChange}
                        />
                    </li>
                ))}
            </ul>
        </div>
    )
}

export default ServerList;