import React, {useContext, useState} from "react";
import './Table.css';
import ServerList from "../ServerList/ServerList";
import {ServersContext} from "../Context/ServersContext";
import Autocomplete from "../Autocomplete/Autocomplete";
import NumberOfServers from "../NumberOfServers/NumberOfServers";

function Table(){
    const {servers} = useContext(ServersContext);
    const [filter, setFilter] = useState('');

    return (
        <div className="main">
            <div className="container">
                <div className="header">
                    <h2>Servers</h2>
                    <NumberOfServers filter={filter} servers={servers}/>
                </div>
                <div>
                    <div className='box-input'>
                        <Autocomplete
                            onFilterChange={setFilter}
                        />
                    </div>
                </div>
            </div>
            {servers && <ServerList filter={filter}/>}
        </div>
    )
}

export default Table;