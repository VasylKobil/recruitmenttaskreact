import React from "react";

export function ServerStatus(props){
    const {status} = props;
    return (
        <div><span className={status.toLowerCase()}>{status === 'REBOOTING' ? 'REBOOTING...' : status}</span></div>
    );
}