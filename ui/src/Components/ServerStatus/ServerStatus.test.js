import React from 'react';
import {render, screen} from "@testing-library/react";
import {ServerStatus} from "./ServerStatus";

it('Server Status Online', () => {
    const { container } = render(<ServerStatus status={'ONLINE'}/>);
    screen.getByText('ONLINE');
    expect(container.firstChild.firstChild.classList.contains('online')).toBe(true);
});

it('Server Status Offline', () => {
    const { container } = render(<ServerStatus status={'OFFLINE'}/>);
    screen.getByText('OFFLINE');
    expect(container.firstChild.firstChild.classList.contains('offline')).toBe(true);
});

it('Server Status Reboot', () => {
    const { container } = render(<ServerStatus status={'REBOOTING'}/>);
    screen.getByText('REBOOTING...');
    expect(container.firstChild.firstChild.classList.contains('rebooting')).toBe(true);
});