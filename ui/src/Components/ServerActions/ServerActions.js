import React, {useRef, useState, useEffect} from 'react';
import './ServerActions.css';
import requests from "../../Services/api";

function ServerActions(props){
    const {serverId, status, onStatusChange} = props;
    const [showBox, setShowBox] = useState(false);
    const ref = useRef(null);

    useEffect(() => {
        function handleClickOutside(event) {
            if (ref.current && !ref.current.contains(event.target)) {
                setShowBox(false);
            }
        }
        document.addEventListener("mousedown", handleClickOutside);
        return () => {
            document.removeEventListener("mousedown", handleClickOutside);
        };
    }, [ref]);

    const turn = () => {
        if (status === 'ONLINE') {
            requests.turnOff(serverId)
                .then((server)=>onStatusChange(server));
        } else {
            requests.turnOn(serverId)
                .then((server)=>onStatusChange(server));
        }

        setShowBox(false);
    };
    const reboot = () => {
        requests.reboot(serverId).then((server)=>onStatusChange(server));
        setShowBox(false);
    }

    return (
        <div ref={ref}>
            <button onClick={() => {setShowBox(true)}}/>
            {showBox &&
                 <div className={"dropdown"}>
                     <button onClick={turn}>{status !== 'ONLINE' ? 'Turn on' : 'Turn off'}</button>
                     {status !== 'OFFLINE' && <button onClick={reboot}>Reboot</button>}
                 </div>
            }
        </div>
    )
}

export default ServerActions;