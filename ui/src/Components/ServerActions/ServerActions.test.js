import {fireEvent, render, screen} from "@testing-library/react";
import React from "react";
import ServerActions from "./ServerActions";

global.fetch = jest.fn(() =>
    Promise.resolve({
        json: () => Promise.resolve({ }),
    })
);

it('Reboot triggers backend api', () => {
    render(<ServerActions serverId={2} status='ONLINE' onStatusChange={()=>null}/>);
    const menuButton = screen.getByRole('button');
    fireEvent.click(menuButton);
    screen.getAllByRole('button');
    const rebootButton = screen.getByText('Reboot');
    fireEvent.click(rebootButton);
    expect(fetch).toHaveBeenCalledWith(
        "http://localhost:4454/servers/2/reboot",
        {"method": "PUT"}
    );
});