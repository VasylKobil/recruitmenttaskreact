import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './Components/App/App';
import ServersContextProvider from "./Components/Context/ServersContext";

ReactDOM.render(
    <React.StrictMode>
        <ServersContextProvider>
            <App />
        </ServersContextProvider>
    </React.StrictMode>
    , document.getElementById('root'));
